EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A4
U 1 1 5EEB92FA
P 8500 1850
F 0 "A4" H 8550 2950 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 8550 2900 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 8775 1100 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 8600 1550 50  0001 C CNN
	1    8500 1850
	1    0    0    -1  
$EndComp
$Comp
L BarduinoStepperBoard-cache:Connector_Conn_01x04_Male A_AXIS4
U 1 1 5EEBB6F5
P 9600 1950
F 0 "A_AXIS4" H 10000 2150 50  0000 R CNN
F 1 "Conn_01x04_Male" H 10150 2250 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9600 1950 50  0001 C CNN
F 3 "" H 9600 1950 50  0001 C CNN
	1    9600 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	9400 1750 9000 1750
Wire Wire Line
	9400 1850 9000 1850
Wire Wire Line
	9400 1950 9000 1950
Wire Wire Line
	9400 2050 9000 2050
$Comp
L Device:CP C1
U 1 1 5EEBEA76
P 9750 1200
F 0 "C1" H 9868 1246 50  0000 L CNN
F 1 "CP" H 9868 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_8x5.4" H 9788 1050 50  0001 C CNN
F 3 "~" H 9750 1200 50  0001 C CNN
	1    9750 1200
	1    0    0    -1  
$EndComp
Text GLabel 8250 2850 0    50   Input ~ 0
GND
$Comp
L fab2020:SLIDE-SWITCH Spindle_Enable1
U 1 1 5EEBFAF3
P 3400 5250
F 0 "Spindle_Enable1" H 3400 4971 45  0000 C CNN
F 1 "SLIDE-SWITCH" H 3400 5055 45  0000 C CNN
F 2 "fab-AYZ0102AGRLC" H 3430 5400 20  0001 C CNN
F 3 "" H 3400 5250 50  0001 C CNN
	1    3400 5250
	-1   0    0    1   
$EndComp
Text GLabel 10150 1050 2    50   Input ~ 0
VMOT
Text GLabel 7650 1150 0    50   Input ~ 0
3V3
Wire Wire Line
	8250 2850 8500 2850
Wire Wire Line
	9750 2850 9750 1350
Wire Wire Line
	9750 1050 10150 1050
Wire Wire Line
	9750 1050 8700 1050
Wire Wire Line
	8700 1050 8700 1150
Connection ~ 9750 1050
Wire Wire Line
	8500 1150 7650 1150
$Comp
L BarduinoStepperBoard-cache:Connector_Generic_Conn_02x03_Odd_Even JP4
U 1 1 5EEC19C1
P 7600 2250
F 0 "JP4" H 7600 2050 50  0000 C CNN
F 1 "Conn_02x03" H 7600 1950 50  0000 C CNN
F 2 "fab:fab-2X03SMD" H 7600 2250 50  0001 C CNN
F 3 "" H 7600 2250 50  0001 C CNN
	1    7600 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2150 8100 2150
Wire Wire Line
	7900 2250 8100 2250
Wire Wire Line
	7900 2350 8100 2350
Wire Wire Line
	7400 2350 7400 2250
Wire Wire Line
	7400 2250 7400 2150
Connection ~ 7400 2250
Text GLabel 6900 2250 0    50   Input ~ 0
3V3
Wire Wire Line
	7400 2250 6900 2250
Wire Wire Line
	8100 1550 7950 1550
Wire Wire Line
	7950 1550 7950 1450
Wire Wire Line
	7950 1450 8100 1450
Text GLabel 7650 1750 0    50   Input ~ 0
Step_Enable
Text GLabel 7450 1850 0    50   Input ~ 0
A_Step
Text GLabel 7400 1950 0    50   Input ~ 0
A_Dir
Wire Wire Line
	8100 1750 7650 1750
Wire Wire Line
	8100 1850 7450 1850
Wire Wire Line
	8100 1950 7400 1950
Wire Wire Line
	8700 2650 8700 2850
Connection ~ 8700 2850
Wire Wire Line
	8700 2850 9750 2850
Wire Wire Line
	8500 2650 8500 2850
Connection ~ 8500 2850
Wire Wire Line
	8500 2850 8700 2850
$Comp
L Connector:Barrel_Jack_Switch J2
U 1 1 5EECAFDA
P 3250 1500
F 0 "J2" H 3307 1817 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 3307 1726 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Wuerth_6941xx301002" H 3300 1460 50  0001 C CNN
F 3 "~" H 3300 1460 50  0001 C CNN
	1    3250 1500
	1    0    0    -1  
$EndComp
Text GLabel 3750 1400 2    50   Input ~ 0
VMOT
Text GLabel 3750 1600 2    50   Input ~ 0
GND
Wire Wire Line
	3750 1600 3650 1600
Wire Wire Line
	3550 1500 3650 1500
Wire Wire Line
	3650 1500 3650 1600
Connection ~ 3650 1600
Wire Wire Line
	3650 1600 3550 1600
Wire Wire Line
	3750 1400 3550 1400
$Comp
L BarduinoStepperBoard-cache:Connector_Conn_01x04_Male PinINPUT1
U 1 1 5EECD521
P 3950 2800
F 0 "PinINPUT1" H 4350 3000 50  0000 R CNN
F 1 "Conn_01x04_Male" H 4500 3100 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3950 2800 50  0001 C CNN
F 3 "" H 3950 2800 50  0001 C CNN
	1    3950 2800
	-1   0    0    1   
$EndComp
Text GLabel 3150 2600 0    50   Input ~ 0
3V3
Text GLabel 3450 2700 0    50   Input ~ 0
Step_Enable
Text GLabel 3250 2800 0    50   Input ~ 0
A_Step
Text GLabel 3200 2900 0    50   Input ~ 0
A_Dir
Wire Wire Line
	3200 2900 3750 2900
Wire Wire Line
	3750 2800 3250 2800
Wire Wire Line
	3750 2700 3450 2700
Wire Wire Line
	3750 2600 3150 2600
$Comp
L BarduinoStepperBoard-rescue:RES-US1206FAB-fab R1
U 1 1 5EED55DB
P 2550 5150
F 0 "R1" H 2550 5344 45  0000 C CNN
F 1 "100Ω" H 2550 5260 45  0000 C CNN
F 2 "fab-R1206FAB" H 2580 5300 20  0001 C CNN
F 3 "" H 2550 5150 50  0001 C CNN
	1    2550 5150
	1    0    0    -1  
$EndComp
Text GLabel 2100 5150 0    50   Input ~ 0
3V3
Wire Wire Line
	2100 5150 2350 5150
Wire Wire Line
	2750 5150 3200 5150
Text GLabel 2100 5350 0    50   Input ~ 0
GND
Wire Wire Line
	3200 5350 2100 5350
Text GLabel 4500 5250 2    50   Input ~ 0
Spindle_Ena
Wire Wire Line
	3850 5250 3600 5250
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A5
U 1 1 5EEDF422
P 8550 4600
F 0 "A5" H 8600 5700 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 8600 5650 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 8825 3850 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 8650 4300 50  0001 C CNN
	1    8550 4600
	1    0    0    -1  
$EndComp
$Comp
L BarduinoStepperBoard-cache:Connector_Conn_01x04_Male C_AXIS5
U 1 1 5EEDF428
P 9650 4700
F 0 "C_AXIS5" H 10050 4900 50  0000 R CNN
F 1 "Conn_01x04_Male" H 10200 5000 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9650 4700 50  0001 C CNN
F 3 "" H 9650 4700 50  0001 C CNN
	1    9650 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9450 4500 9050 4500
Wire Wire Line
	9450 4600 9050 4600
Wire Wire Line
	9450 4700 9050 4700
Wire Wire Line
	9450 4800 9050 4800
$Comp
L Device:CP C2
U 1 1 5EEDF432
P 9800 3950
F 0 "C2" H 9918 3996 50  0000 L CNN
F 1 "CP" H 9918 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_8x5.4" H 9838 3800 50  0001 C CNN
F 3 "~" H 9800 3950 50  0001 C CNN
	1    9800 3950
	1    0    0    -1  
$EndComp
Text GLabel 8300 5600 0    50   Input ~ 0
GND
Text GLabel 10200 3800 2    50   Input ~ 0
VMOT
Text GLabel 7700 3900 0    50   Input ~ 0
3V3
Wire Wire Line
	8300 5600 8550 5600
Wire Wire Line
	9800 5600 9800 4100
Wire Wire Line
	9800 3800 10200 3800
Wire Wire Line
	9800 3800 8750 3800
Wire Wire Line
	8750 3800 8750 3900
Connection ~ 9800 3800
Wire Wire Line
	8550 3900 7700 3900
$Comp
L BarduinoStepperBoard-cache:Connector_Generic_Conn_02x03_Odd_Even JP5
U 1 1 5EEDF442
P 7650 5000
F 0 "JP5" H 7650 4800 50  0000 C CNN
F 1 "Conn_02x03" H 7650 4700 50  0000 C CNN
F 2 "fab:fab-2X03SMD" H 7650 5000 50  0001 C CNN
F 3 "" H 7650 5000 50  0001 C CNN
	1    7650 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 4900 8150 4900
Wire Wire Line
	7950 5000 8150 5000
Wire Wire Line
	7950 5100 8150 5100
Wire Wire Line
	7450 5100 7450 5000
Wire Wire Line
	7450 5000 7450 4900
Connection ~ 7450 5000
Text GLabel 6950 5000 0    50   Input ~ 0
3V3
Wire Wire Line
	7450 5000 6950 5000
Wire Wire Line
	8150 4300 8000 4300
Wire Wire Line
	8000 4300 8000 4200
Wire Wire Line
	8000 4200 8150 4200
Text GLabel 7700 4500 0    50   Input ~ 0
Step_Enable
Text GLabel 7500 4600 0    50   Input ~ 0
C_Step
Text GLabel 7450 4700 0    50   Input ~ 0
C_Dir
Wire Wire Line
	8150 4500 7700 4500
Wire Wire Line
	8150 4600 7500 4600
Wire Wire Line
	8150 4700 7450 4700
Wire Wire Line
	8750 5400 8750 5600
Connection ~ 8750 5600
Wire Wire Line
	8750 5600 9800 5600
Wire Wire Line
	8550 5400 8550 5600
Connection ~ 8550 5600
Wire Wire Line
	8550 5600 8750 5600
$Comp
L BarduinoStepperBoard-cache:Connector_Conn_01x04_Male PinINPUT2
U 1 1 5EEE91BD
P 4000 3500
F 0 "PinINPUT2" H 4400 3700 50  0000 R CNN
F 1 "Conn_01x04_Male" H 4550 3800 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4000 3500 50  0001 C CNN
F 3 "" H 4000 3500 50  0001 C CNN
	1    4000 3500
	-1   0    0    1   
$EndComp
Text GLabel 3500 3400 0    50   Input ~ 0
Step_Enable
Text GLabel 3300 3500 0    50   Input ~ 0
C_Step
Text GLabel 3250 3600 0    50   Input ~ 0
C_Dir
Wire Wire Line
	3250 3600 3800 3600
Wire Wire Line
	3800 3500 3300 3500
Wire Wire Line
	3800 3400 3500 3400
Text GLabel 3500 3300 0    50   Input ~ 0
Spindle_Ena
Wire Wire Line
	3800 3300 3500 3300
$Comp
L BarduinoStepperBoard-rescue:RES-US1206FAB-fab R0
U 1 1 5EF15678
P 4050 5250
F 0 "R0" H 4050 5444 45  0000 C CNN
F 1 "0Ω" H 4050 5360 45  0000 C CNN
F 2 "fab-R1206FAB" H 4080 5400 20  0001 C CNN
F 3 "" H 4050 5250 50  0001 C CNN
	1    4050 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5250 4500 5250
Text Notes 3200 1050 0    50   ~ 0
Power
Text Notes 3200 2350 0    50   ~ 0
INput
Text Notes 3150 4550 0    50   ~ 0
OUTput
Wire Notes Line
	1850 900  1850 5550
Wire Notes Line
	1850 5550 5050 5550
Wire Notes Line
	5050 900  1850 900 
Wire Notes Line
	1850 1950 5050 1950
Wire Notes Line
	1850 4350 5050 4350
Wire Notes Line
	5050 900  5050 5550
$EndSCHEMATC
