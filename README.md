
## This is a fork from [barduino-cnc-shield](https://gitlab.com/fablabbcn-projects/electronics/barduino-cnc-shield)
we tend to adapt the shield to use it for [5axis CNC SPML]((https://gitlab.com/armannd092/spml-5axies) which also is a child of [SPML CNC](https://gitlab.com/fablabbcn-projects/cnc-machines/six-pack-cnc/-/tree/master/).

This is a working in progress most of the following documentation inherit from the original repo.

# Barduino 5Axis CNC shield

This shield is intended to be used with a Barduino 2.3 (micro usb version) and with [SPML](https://gitlab.com/armannd092/spml-5axies). The files need slight modification to be made in a fablab as a double side copper (change the size of the vias).

![](img/TopXBarduino.jpg)
![](img/Top.jpg)
![](exports/front.png)

## Functionalities

This board is a fork of [bdring's](bdring) [Grbl ESP32 Development Controller](https://github.com/bdring/Grbl_ESP32_Development_Controller). The adaptation's here are done for usage with a custom Barduino and A4988 drivers. Functionalities are:

- 3 Drivers for stepper motor with selectable microstepping
- Barrel jack connection for power with buck converter
- 6 + 5 digital inputs
- Spindle PWM control for external controller at 3V or 5V
- Mosfet PWM output

![](img/pcblayout.jpg)

## BOM

Find the bom in `exports/BOM.csv`.

## Protection Case

This component is meant to be 3d printed in an standart 0.4mm Fdm printer at 0.3mm layer height, supports needed on "basefile"

![](img/case.jpg)

Files:
- [3d rhino file](case-3dfiles/3DPRINT-CncShieldCase.3dm)
- [Stl top case loose fit](case-3dfiles/3DPRINT-CncShieldTopCase.stl)
- [Stl top case tight fit](case-3dfiles/3DPRINT-CncShieldTopCaseTightFit.stl)
- [Stl base case](case-3dfiles/3DPRINT-CncShieldBaseCase.stl)

## Firmware - GRBLESP32

This shield was designed to work together with the Esp32 Barduino, which you can find in this [link](https://gitlab.com/fablabbcn-projects/electronics/barduino)

This electronic hardware combo was designed around the idea of using a forked version of GBRL that is compatible with esp32 and adds lot of functionalities. The firmware version you will find in this repo has already been modified to match this hardware.

This version will selfPublish web interface to control the machine through the brower via wifi or bluetooth throught a cncgbrl app.

![](https://raw.githubusercontent.com/luc-github/ESP3D-WEBUI/master/images/Full1.PNG)

[COMPLETE INFORMATION OF THE GRBLESP32 ORIGINAL WIKI](https://github.com/bdring/Grbl_Esp32/wiki/Development-Roadmap)

[COMPLETE INFORMATION OF THE WEB UI CONTROL - ORIGINAL REPO](https://github.com/luc-github/ESP3D-WEBUI)

The ESP32 is potentially a great target for Grbl for the following reasons

- Faster - At least 4x the step rates over Grbl
- Lower Cost
- Small footprint
- More Flash and RAM - A larger planner buffer could be used and more features could be added.
- I/O - It has just about the same number of pins as an Arduino UNO, the original target for Grbl
- Peripherals - It has more timers and advanced features than an UNO. These can also be mapped to pins more flexibly.
- Connectivity - Bluetooth and WiFi built in.
- Fast Boot - Boots almost instantly and does not need to be formally shutdown (unlike Raspberry Pi or Beagle Bone)
- RTOS (Real Time operating System) - Custom features can be added without affecting the performance of the motion control system.

The code should be compiled using the latest Arduino IDE. [Follow instructions](https://github.com/espressif/arduino-esp32) here on how to setup ESP32 in the IDE. The choice was made to use the Arduino IDE over the ESP-IDF to make the code a little more accessible to novices trying to compile the code.

I use the ESP32 Dev Module version of the ESP32. I suggest starting with that if you don't have hardware yet.

For basic instructions on using Grbl use the [gnea/grbl wiki](https://github.com/gnea/grbl/wiki). That is the Arduino version of Grbl, so keep that in mind regarding hardware setup.
